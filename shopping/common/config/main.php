<?php
return [
    'aliases' => [
        '@bower' => '@vendor/bower-asset',
        '@npm'   => '@vendor/npm-asset',
    ],
    'language' => 'fa-IR',
    'timeZone' => 'Asia/Tehran',
    'vendorPath' => dirname(dirname(__DIR__)) . '/vendor',
    'components' => [
        'formatter' => [
            'locale' => 'fa_IR@calendar=persian',
            'calendar' => IntlDateFormatter::TRADITIONAL,
            'dateFormat' => 'yyyy/MM/dd hh:mm:ss',
        ],
        'cache' => [
            'class' => 'yii\caching\FileCache',
        ],
    ],
];
