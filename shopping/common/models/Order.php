<?php

namespace common\models;

use Yii;

/**
 * This is the model class for table "order".
 *
 * @property int $order_id
 * @property int $user_id_fk
 * @property int $product_id_fk
 * @property int $order_price
 * @property int $order_status
 * @property string $order_cdt
 *
 * @property User $userIdFk
 * @property Product $product
 */
class Order extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'order';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['user_id_fk', 'order_price', 'product_id_fk'], 'required'],
            [['user_id_fk', 'product_id_fk', 'order_price', 'order_status'], 'integer'],
            [['order_cdt'], 'safe'],
            [['user_id_fk'], 'exist', 'skipOnError' => true, 'targetClass' => User::className(), 'targetAttribute' => ['user_id_fk' => 'user_id']],
            [['product_id_fk'], 'exist', 'skipOnError' => true, 'targetClass' => Product::className(), 'targetAttribute' => ['product_id_fk' => 'product_id']],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'order_id' => 'Order ID',
            'user_id_fk' => 'User Id Fk',
            'order_price' => 'Order Price',
            'order_status' => 'Order Status',
            'order_cdt' => 'Order Cdt',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getUserIdFk()
    {
        return $this->hasOne(User::className(), ['user_id' => 'user_id_fk']);
    }

    public function product() {
        return Product::findOne($this->product_id_fk);
    }
}
