<?php

namespace common\models;

use common\components\PersianHelper;
use Yii;

/**
 * This is the model class for table "product".
 *
 * @property int $product_id
 * @property string $product_name
 * @property string $product_desc
 * @property int $product_price
 * @property int $product_status
 * @property string $product_cdt
 * @property string $product_udt
 */
class Product extends \yii\db\ActiveRecord
{
    const STATUS_ACTIVE = 1;
    const STATUS_INACTIVE = 2;
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'product';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['product_name', 'product_desc', 'product_price'], 'required'],
            [['product_price', 'product_status'], 'integer'],
            [['product_cdt', 'product_udt'], 'safe'],
            [['product_name'], 'string', 'max' => 256],
            [['product_desc'], 'string', 'max' => 2048],
        ];
    }

    public function beforeSave($insert)
    {
        $this->product_price = PersianHelper::EnDigitToFaDigit($this->product_price);
        return parent::beforeSave($insert);
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'product_id' => 'شناسه محصول',
            'product_name' => 'نام',
            'product_desc' => 'توضیحات',
            'product_price' => 'قیمت',
            'product_status' => 'وضعیت',
            'product_cdt' => 'تاریخ ایجاد',
            'product_udt' => 'تاریخ به روز رسانی',
        ];
    }
}
