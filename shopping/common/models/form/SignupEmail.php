<?php

namespace common\models\form;

use common\components\PersianHelper;
use common\components\TextMessage;
use common\models\User;
use Yii;
use yii\base\Model;

/**
 * User Mobile Login form
 */
class SignupEmail extends Model
{
    public $mobile;
    public $email;
    public $password;
    public $name;
    public $family;

    /** @var $_user \common\models\User */
    private $_user;

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            ['name', 'trim'],
            ['name', 'required'],
            ['name', 'string', 'min' => 2, 'max' => 255],

            ['family', 'required'],
            ['family', 'string', 'min' => 2, 'max' => 255],

            ['email', 'trim'],
            ['email', 'required'],
            ['email', 'email'],
            ['email', 'string', 'max' => 255],
            ['email', 'unique', 'targetClass' => '\common\models\User', 'message' => 'این ایمیل قبلا در سیستم ثبت شده است.', 'targetAttribute' => 'user_email'],

            ['mobile', 'validateMobile'],

            ['password', 'required'],
            ['password', 'string', 'min' => 6],
        ];
    }

    /**
     * @return array
     */
    public function attributeLabels()
    {
        return [
            'mobile' => 'شماره تلفن همراه',
            'name' => 'نام',
            'family' => 'نام خانوادگی',
            'email' => 'ایمیل',
            'password' => 'پسورد'
        ];
    }

    public function validateMobile($attribute)
    {
        $this->mobile = PersianHelper::phoneFormat(PersianHelper::faDigitToEnDigit($this->mobile));
        if (!$this->hasErrors()) {
            $this->_user = $this->getUser();

            if ($this->_user) {
                $this->addError($attribute, 'شماره موبایل وارد شده در سیستم ثبت شده است.');
                return false;
            }
        }

        return true;
    }

    /**
     * @return bool|\common\models\User
     * @throws \yii\base\Exception
     */
    public function signup()
    {
        if (!$this->validate()) {
            return false;
        }

        $user = new \common\models\User();
        $user->user_mobile = $this->mobile;
        $user->user_email = $this->email;
        $user->user_name = $this->name;
        $user->user_family = $this->family;
        $user->generateAuthKey();
        $user->setPassword($this->password);

        if (!$user->save()) {
            Yii::error($user->errors);
            return false;
        }

        return $user;
    }

    /**
     * @return array|bool|User|null
     */
    protected function getUser()
    {
        $user = User::findByMobile($this->mobile);

        if (!$user) {
            $this->_user = null;
            return false;
        }

        return $user;
    }

}
