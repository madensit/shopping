<?php

use common\components\PersianHelper;
use yii\helpers\Html;
use yii\grid\GridView;

/* @var $this yii\web\View */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'محصولات خریداری شده';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="product-index">

    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],

            [
                'attribute' => 'شناسه محصول',
                'value' => function($model) {
                    return $model->product()->product_id;
                }
            ],
            [
                'attribute' => 'نام محصول',
                'value' => function($model) {
                    return $model->product()->product_name;
                }
            ],
            [
                'attribute' => 'توضیحات محصول',
                'value' => function($model) {
                    return $model->product()->product_desc;
                }
            ],
            [
                'attribute' => 'مبلغ سفارش',
                'value' => function($model) {
                    return PersianHelper::EnDigitToFaDigit($model->product()->product_price) . ' تومان';
                }
            ],
        ],
    ]); ?>


</div>
