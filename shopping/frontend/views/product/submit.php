<?php

use yii\helpers\Html;
use yii\helpers\Url;
use yii\widgets\DetailView;
use \common\components\PersianHelper;

/* @var $this yii\web\View */
/* @var $product common\models\Product */

$this->title = $product->product_name;
$this->params['breadcrumbs'][] = ['label' => 'خرید', 'url' => \yii\helpers\Url::home()];
$this->params['breadcrumbs'][] = $this->title;
\yii\web\YiiAsset::register($this);
?>
<div class="product-view">

    <div style="font-size: 20px;margin-bottom: 20px">
        شما در حال خرید <b><?= $product->product_name ?></b> به قیمت <b><?= PersianHelper::EnDigitToFaDigit($product->product_price) ?> تومان</b> هستید.
    </div>
    <p><a class="btn btn-primary" href="<?= Url::to(['product/submit', 'id' => $product->product_id]) ?>">اتمام خرید</a></p>

</div>
