<?php

use yii\helpers\Html;
use yii\widgets\DetailView;
use \common\components\PersianHelper;

/* @var $this yii\web\View */
/* @var $model common\models\Product */

$this->title = $model->product_name;
$this->params['breadcrumbs'][] = ['label' => 'محصولات', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
\yii\web\YiiAsset::register($this);
?>
<div class="product-view">

    <p>
        <?= Html::a('به روز رسانی', ['update', 'id' => $model->product_id], ['class' => 'btn btn-primary']) ?>
        <?= Html::a('حذف', ['delete', 'id' => $model->product_id], [
            'class' => 'btn btn-danger',
            'data' => [
                'confirm' => 'Are you sure you want to delete this item?',
                'method' => 'post',
            ],
        ]) ?>
    </p>

    <?= DetailView::widget([
        'model' => $model,
        'attributes' => [
            'product_id',
            'product_name',
            'product_desc',
            [
                'attribute' => 'product_price',
                'value' => function($model) {
                    return PersianHelper::EnDigitToFaDigit($model->product_price) . ' تومان';
                }
            ],
        ],
    ]) ?>

</div>
