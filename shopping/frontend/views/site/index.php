<?php

/* @var $this yii\web\View */
/* @var $products[] \common\models\Product */
/* @var $product \common\models\Product */

use common\components\PersianHelper;
use yii\helpers\Url;
$this->title = 'My Yii Application';
?>
<div class="site-index">

    <div class="body-content">

        <div class="row">
            <?php foreach ($products as $product): ?>
            <div class="col-lg-4">
                <div class="bg-primary" style="padding: 10px">
                    <h2><?= $product->product_name ?></h2>

                    <p><?= $product->product_desc ?></p>
                    <p><?= PersianHelper::EnDigitToFaDigit($product->product_price) ?> تومان</p>

                    <p><a class="btn btn-default" href="<?= Url::to(['product/buy', 'id' => $product->product_id]) ?>"> خرید&raquo; </a></p>
                </div>
            </div>
            <?php endforeach; ?>
        </div>

    </div>
</div>
